#include <stdio.h>

#include "main.h"
#include "mem.h"
#include "mem_internals.h"

#include <assert.h>

#define HEAP_SIZE 1024

void debug(const char *fmt, ...);


void testing1() {
    debug("Тест 1: Инициализация кучи\n",  HEAP_SIZE);
    void *heap = heap_init( HEAP_SIZE);

    if (!heap) {
        debug("Тест 1: Не удалось инициализировать кучу.\n");
        assert(false);
    }

    debug_heap(stdout, heap);
    debug("Тест 1: Куча успешно инициализирована.\n");

    debug("Тест 1: Завершение работы с кучей.\n");
    heap_term();

}



void testing2() {
    debug("Тест 2: Инициализация кучи\n",  HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    void* block1 = _malloc(200);
    assert(block1 != NULL);
    void* block2 = _malloc(300);
    assert(block2 != NULL);


    debug("Куча до освобождения:\n");
    debug_heap(stdout, heap);

    _free(block2);

    debug("Куча после освобождения одного блока:\n");
    debug_heap(stdout, heap);
    _free(block1);

    debug_heap(stdout, heap);

    heap_term();

    debug("Тест 2: Завершение работы с кучей.\n");
}



void testing3() {
    debug("Тест 3: Инициализация кучи\n",  HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    void* block1 = _malloc(200);
    void* block2 = _malloc(300);
    void* block3 = _malloc(400);

    debug("Куча до освобождения:\n");
    debug_heap(stdout, heap);

    _free(block2);
    _free(block1);
    debug("Куча после освобождения двух блоков:\n");
    debug_heap(stdout, heap);

    _free(block3);

    debug("Куча после освобождения всех блоков:\n");
    debug_heap(stdout, heap);

    heap_term();

    debug("Тест 3: Завершение работы с кучей.\n");
}


void testing4() {
    debug("Тест 4: Инициализация кучи\n",  HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    void* little_block = _malloc(64);
    debug_heap(stdout, heap);


    void* large_block = _malloc(HEAP_SIZE * 4);
    debug_heap(stdout, heap);

    _free(little_block);
    _free(large_block);
    debug_heap(stdout, heap);


    heap_term();

    debug("Тест 4: Завершение работы.\n");
}


void testing5() {
    debug("Тест 5: Инициализация кучи\n",  HEAP_SIZE);
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    void* block1 = _malloc(HEAP_SIZE / 2);
    void* block2 = _malloc(HEAP_SIZE);
    debug_heap(stdout, heap);


    debug("Освобождение блока 1\n");
    _free(block1);
    debug_heap(stdout, heap);

    debug("Освобождение блока 2, который не был выделен\n");
    _free(block2);
    debug_heap(stdout, heap);

    heap_term();
    debug("Тест 5: Завершение работы.\n");
}

static void testing(checked t) {
    t();
}


int main() {
    debug("Запуск тестов...\n");

    testing(&testing1);
    printf("Первый тест пройден\n");

    testing(&testing2);
    printf("Второй тест пройден\n");

    testing(&testing3);
    printf("Третий тест пройден\n");

    testing(&testing4);
    printf("Четвертый тест пройден\n");

    testing(&testing5);
    printf("Пятый тест пройден\n");

    debug("Тесты прошли.\n");
    return 0;
}
